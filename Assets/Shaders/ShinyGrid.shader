﻿Shader "Custom/ShinyGrid" 
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _GridColour ("Grid Colour", Color) = (0.5, 0.5, 0.5, 0.5)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _GridStep ("Grid size", Float) = 10
        _GridWidth ("Grid width", Float) = 1
        _EmissionColor("Color", Color) = (0,0,0)
        _EmissionMap("Emission", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200
       
        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard //fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0
 
        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
        };
 
        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        float _GridStep;
        float _GridWidth;
        uniform float4 _GridColour;
       
        void surf (Input IN, inout SurfaceOutputStandard o) {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;

            // grid overlay
            float3 pos = IN.worldPos / _GridStep;
            float3 f  = abs(frac(pos)-0.5);
            float3 df = fwidth(pos) * _GridWidth;
            float3 g = smoothstep(-df , df , f);
            float grid = 1 - saturate(g.x * g.y * g.z);
            c.rgb = lerp(c.rgb, _GridColour, grid);

            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}