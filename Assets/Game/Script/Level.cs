﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

	public GameObject section;

	// Use this for initialization
	void Start () {
		Vector3 start = Vector3.zero;
		Vector3 forward = new Vector3(0.5f, 0, 0.5f);
		for(int i = 0; i < 50; ++i)
		{
			GameObject newGO = Instantiate(section);
			newGO.transform.position = start;
			start += forward * 5;
			newGO.transform.eulerAngles = new Vector3(0,45,0);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
