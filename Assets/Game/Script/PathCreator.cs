﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathCreator : MonoBehaviour {

	[HideInInspector]
	public Curve curve;

	public Transform startPoint;
	public Transform endPoint;

	public Color anchorColor = Color.red;
	public Color controlColor = Color.white;
	public Color segmentColor = Color.green;
	public Color selectedSegmentColor = Color.yellow;
	public float anchorDiameter = 0.1f;
	public float controlDiameter = 0.075f;
	public bool displayControlPoints = true;

	public void OnInit()
	{
		//CreatePath();
		//transform.position = Vector3.zero;
		Vector3 pos = transform.position;
		Vector3 rot = transform.eulerAngles;

		startPoint.position = RotateAroundPivot(curve[0] + pos, pos, rot);
		endPoint.position = RotateAroundPivot(curve[curve.NumPoints - 1] + pos, pos, rot);
		endPoint.eulerAngles = rot;
	}

	Vector3 RotateAroundPivot(Vector3 point, Vector3 pivot, Vector3 angle)
	{
		Vector3 dir = point - pivot;
		dir = Quaternion.Euler(angle) * dir;
		return dir + pivot;
	}

	public void CreatePath()
	{
		curve = new Curve(transform.position + Vector3.forward * 10);
	}

	public void Reset()
	{
		CreatePath();
	}
}
