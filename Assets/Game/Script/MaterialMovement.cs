﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialMovement : MonoBehaviour {

    public Material mat;
    float value;
    public float maxValue;
	// Use this for initialization
	void Start () {
        maxValue = 3;
	}
	
	// Update is called once per frame
	void Update () {

        value = Mathf.Abs(Mathf.Sin(Time.time) * maxValue);
        mat.mainTextureOffset += new Vector2(2.0f, 0) * Time.deltaTime;

        //0.7517242
        mat.SetColor("_EmissionColor", new Vector4(0, 0.7517242f, 1, 0) * value);
    }
}
