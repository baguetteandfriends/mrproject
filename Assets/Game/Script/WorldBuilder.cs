﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBuilder : MonoBehaviour {

	public enum Spawnables
	{
		SingleLaneWalled = 0,
		SingleLaneSplit = 1,
		SingleLaneCurve = 2,
	}

	public Vector3 fallbackSpawnPoint;
	public Transform parent;
	public GameObject currentObject;
	private int counter = 0;

	public GameObject[] spawnableObjects;

	public Spawnables spawnIndex;

	public void GetFirst()
	{
		if(transform.childCount == 0)
		{
			Debug.LogError("There are no children! Add one");
			return;
		}
		currentObject = transform.GetChild(0).gameObject;
	}

	public void GetLast()
	{
		int count = transform.childCount;
		if (count == 0)
		{
			Debug.LogError("There are no children! Add one");
			return;
		}
		currentObject = transform.GetChild(count - 1).gameObject;
	}

	public void BuildObject(int idx)
	{
		Vector3 spawnPoint = fallbackSpawnPoint;
		Quaternion rotation = Quaternion.identity;
		BuildableObject oldBO = null;
		if(currentObject != null)
		{
			oldBO = currentObject.GetComponent<BuildableObject>();
			spawnPoint = oldBO.EndPoints[idx].position;
			rotation = oldBO.EndPoints[idx].rotation;
		}

		var GO = Instantiate(spawnableObjects[(int)spawnIndex], 
			spawnPoint, rotation, parent) as GameObject;

		GO.name += counter.ToString();
		counter++;

		if(oldBO != null)
		{
			oldBO.ForwardObject.Add(GO);
			BuildableObject newBO = GO.GetComponent<BuildableObject>();
			newBO.BackwardObject.Add(currentObject);
		}

		if(spawnIndex == Spawnables.SingleLaneCurve)
		{
			GO.GetComponent<PathCreator>().OnInit();
			//GO.transform.position = Vector3.zero;
			GO.GetComponent<FloorCreator>().UpdateMesh();
			GO.GetComponent<FloorCreator>().UpdateColliders();
		}

		currentObject = GO;
	}

	public void MoveBackwards(int idx)
	{
		if(currentObject == null)
		{
			Debug.LogError("Current Object not set");
			return;
		}

		BuildableObject oldBO = currentObject.GetComponent<BuildableObject>();

		if(oldBO == null)
		{
			Debug.LogError("Missing Buildable Object script");
			return;
		}

		if(oldBO.BackwardObject.Count == 0)
		{
			Debug.LogWarning("There is no backwards object");
			return;
		}

		currentObject = oldBO.BackwardObject[idx];
		// Make the inspector
		//UnityEditor.Selection.activeObject = currentObject;
	}

	public void MoveForwards(int idx)
	{
		if (currentObject == null)
		{
			Debug.LogError("Current Object not set");
			return;
		}

		BuildableObject oldBO = currentObject.GetComponent<BuildableObject>();

		if (oldBO == null)
		{
			Debug.LogError("Missing Buildable Object script");
			return;
		}

		if (oldBO.ForwardObject.Count == 0)
		{
			Debug.LogWarning("There is no forwards object");
			return;
		}

		currentObject = oldBO.ForwardObject[idx];
		//UnityEditor.Selection.activeObject = currentObject;
	}

	public void DeleteObject()
	{

	}

	public void DeleteRecursive()
	{

	}

	public void GoToObject()
	{
		if(currentObject == null) Debug.LogError("No Current Object");
		else UnityEditor.Selection.activeObject = currentObject;
	}
}
