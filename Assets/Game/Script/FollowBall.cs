﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBall : MonoBehaviour {

	public Transform myTransform;
	public Transform ball;
	public Vector3 offset;
	public Vector3 angles;

	// Use this for initialization
	void Start()
	{
		myTransform = transform;
	}

	// Update is called once per frame
	void Update()
	{

		myTransform.position = new Vector3(myTransform.position.x, myTransform.position.y, ball.position.z + offset.z);
		myTransform.eulerAngles = angles;

	}
}