﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WorldBuilder))]
public class WorldBuilderEditor : Editor {

	WorldBuilder script;
	public int childCount;
	private const int spaceDistance = 20;

	public void OnEnable()
	{
		script = (WorldBuilder)target;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		childCount = script.transform.childCount;
		GameObject go = script.currentObject;

		if(go == null)
		{
			if(GUILayout.Button("Build Object"))
			{
				Undo.RecordObject(script, "Build Object");
				script.BuildObject(0);
			}
		}
		else
		{
			BuildableObject bo = go.GetComponent<BuildableObject>();
			for(int i = 0; i < bo.EndPoints.Count; ++i)
			{
				if(GUILayout.Button("Build Object at Idx " + i))
				{
					Undo.RecordObject(script, "Build Object");
					script.BuildObject(i);
				}
			}
		}

		GUILayout.Space(spaceDistance);

		if(childCount > 0)
		{
			if(GUILayout.Button("Get First Object"))
			{
				Undo.RecordObject(script, "Get First Object");
				script.GetFirst();
			}

			if(GUILayout.Button("Get Last Object"))
			{
				Undo.RecordObject(script, "Get Last Object");
				script.GetLast();
			}

			GUILayout.Space(spaceDistance);

			if(GUILayout.Button("Delete Object"))
			{
				Undo.RecordObject(script, "Delete Object");
				script.DeleteObject();
			}

			if (GUILayout.Button("Delete Object Recursive"))
			{
				Undo.RecordObject(script, "Delete Object Recursive");
				script.DeleteRecursive();
			}

			GUILayout.Space(spaceDistance);

			if(GUILayout.Button("Go to Current Object"))
			{
				Undo.RecordObject(script, "Go to Object");
				script.GoToObject();
			}

			GUILayout.Space(spaceDistance);

			if (childCount > 1 && script.currentObject != null)
			{
				BuildableObject bo = script.currentObject.GetComponent<BuildableObject>();
				for(int i = 0; i < bo.BackwardObject.Count; ++i)
				{
					string bk = "Move Backwards Child ";
					if (GUILayout.Button(bk + i.ToString()))
					{
						Undo.RecordObject(script, "Go Backwards");
						script.MoveBackwards(i);
					}
				}

				for(int i = 0; i < bo.ForwardObject.Count; ++i)
				{
					string fd = "Move Forwards Child ";
					if (GUILayout.Button(fd + i.ToString()))
					{
						Undo.RecordObject(script, "Go Forwards");
						script.MoveForwards(i);
					}
				}
			}
		}
	}

}
