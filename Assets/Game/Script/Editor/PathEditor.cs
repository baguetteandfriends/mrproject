﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PathCreator))]
public class PathEditor : Editor {

	PathCreator creator;
	Curve curve
	{
		get
		{
			return creator.curve;
		}
	}

	const float segmentSelectDistanceThreshold = 0.1f;
	int selectedSegmentIndex = -1;

	public float hitDistance = 10;

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		EditorGUI.BeginChangeCheck();

		if (GUILayout.Button("Create New"))
		{
			Undo.RecordObject(creator, "Create New");
			creator.CreatePath();
		}

		if(GUILayout.Button("Zero All Y values"))
		{
			Undo.RecordObject(creator, "Zero Y");
			curve.ZeroY();
		}

		bool isClosed = GUILayout.Toggle(curve.IsClosed, "Closed");
		if (isClosed != curve.IsClosed)
		{
			Undo.RecordObject(creator, "Toggle Closed");
			curve.IsClosed = isClosed;
		}

		bool autoSetControlPoints = GUILayout.Toggle(
			curve.AutoSetControlPoints, "Auto Set Control Points");
		if(autoSetControlPoints != curve.AutoSetControlPoints)
		{
			Undo.RecordObject(creator, "Toggle Auto Set Control Points");
			curve.AutoSetControlPoints = autoSetControlPoints;
		}

		Vector3 pos = creator.transform.position;
		Vector3 rot = creator.transform.eulerAngles;

		creator.startPoint.position = RotateAroundPivot(
			curve[0] + pos, pos, rot);
		creator.endPoint.position = RotateAroundPivot(
			curve[curve.NumPoints - 1] + pos, pos, rot);
		creator.endPoint.eulerAngles = rot;
		
		if (EditorGUI.EndChangeCheck())
		{
			SceneView.RepaintAll();
		}
	}

	void OnSceneGUI()
	{
		Input();
		Draw();
	}

	void Input()
	{
		Event guiEvent = Event.current;
		Ray r = HandleUtility.GUIPointToWorldRay(guiEvent.mousePosition);
		
		RaycastHit hit;
		Vector3 mousePos;
		if (Physics.Raycast(r, out hit, hitDistance))
		{
			mousePos = hit.point;
			mousePos.y = 0;
		}
		else
		{
			// Only works in orthgraphic
			mousePos = r.origin;
			mousePos.y = 0;
		}

		if (guiEvent.type == EventType.MouseDown && guiEvent.button == 0 && guiEvent.shift)
		{
			if(selectedSegmentIndex != -1)
			{
				Undo.RecordObject(creator, "Split Segment");
				curve.SplitSegment(mousePos, selectedSegmentIndex);
			}
			else if (!curve.IsClosed)
			{
				Undo.RecordObject(creator, "Add Segment");
				curve.AddSegment(mousePos);
			}
		}

		if(guiEvent.type == EventType.MouseDown && guiEvent.button == 1)
		{
			float minDstToAnchor = creator.anchorDiameter * 0.5f;
			int closestAnchorIndex = -1;
			for (int i = 0; i < curve.NumPoints; i += 3)
			{
				float dist = Vector3.Distance(mousePos, curve[i]);
				if(dist < minDstToAnchor)
				{
					minDstToAnchor = dist;
					closestAnchorIndex = i;
				}
			}

			if(closestAnchorIndex != -1)
			{
				Undo.RecordObject(creator, "Delete Segment");
				curve.DeleteSegment(closestAnchorIndex);
			}
		}

		if(guiEvent.type == EventType.MouseMove)
		{
			float minDistanceToSegment = segmentSelectDistanceThreshold;
			int newSelectedSegmentIndex = -1;
			for(int i = 0; i < curve.NumSegments; ++i)
			{
				Vector3[] points = curve.GetPointsInSegment(i);
				float dist = HandleUtility.DistancePointBezier(mousePos, points[0], points[3], points[1], points[2]);
				if(dist < minDistanceToSegment)
				{
					minDistanceToSegment = dist;
					newSelectedSegmentIndex = i;
				}
			}
			if(newSelectedSegmentIndex != selectedSegmentIndex)
			{
				selectedSegmentIndex = newSelectedSegmentIndex;
				SceneView.RepaintAll();
			}
		}

		HandleUtility.AddDefaultControl(0);
	}

	Vector3 RotateAroundPivot(Vector3 point, Vector3 pivot, Vector3 angle)
	{
		Vector3 dir = point - pivot;
		dir = Quaternion.Euler(angle) * dir;
		return dir + pivot;
	}

	void Draw()
	{
		Vector3 origin = creator.transform.position;
		Vector3 rotation = creator.transform.eulerAngles;

		for(int i = 0; i < curve.NumSegments; ++i)
		{
			Vector3[] points = curve.GetPointsInSegment(i);
			for(int j = 0; j < points.Length; ++j)
			{
				points[j] += origin;
				points[j] = RotateAroundPivot(points[j], origin, rotation);
			}

			if(creator.displayControlPoints)
			{
				Handles.color = Color.black;
				Handles.DrawLine(points[1], points[0]);
				Handles.DrawLine(points[2], points[3]);
			}
			Color segmentCol = (i == selectedSegmentIndex && Event.current.shift) 
				? creator.selectedSegmentColor : creator.segmentColor;
			Handles.DrawBezier(points[0], points[3], points[1], points[2], segmentCol, null, 2);
		}

		Handles.color = Color.red;
		for(int i = 0; i < curve.NumPoints; ++i)
		{
			if(i % 3 == 0 || creator.displayControlPoints)
			{
				Handles.color = (i % 3 == 0) ? creator.anchorColor : creator.controlColor;
				float handleSize = (i % 3 == 0) ? creator.anchorDiameter : creator.controlDiameter;

				Vector3 handlePos = curve[i] + origin;
				handlePos = RotateAroundPivot(handlePos, origin, rotation);

				Vector3 newPos = Handles.FreeMoveHandle(handlePos, Quaternion.identity, handleSize, Vector3.zero, Handles.SphereHandleCap);

				Vector3 undoPos = RotateAroundPivot(newPos, origin, -rotation) - origin;

				if (handlePos != newPos)
				{
					Undo.RecordObject(creator, "Move point");
					curve.MovePoint(i, undoPos);
				}
			}
		}
	}

	void OnEnable()
	{
		creator = (PathCreator)target;
		if(creator.curve == null)
		{
			creator.CreatePath();
		}
	}

}
