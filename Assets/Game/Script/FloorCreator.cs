﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PathCreator))]
public class FloorCreator : MonoBehaviour
{
	[Range(0.05f, 10f)]
	public float spacing = 1;
	public float floorWidth = 1;
	public float tiling = 1;
	public float wallHeight = 2;
	public bool autoUpdate;

	public GameObject floorGO;
	public GameObject leftWallGO;
	public GameObject rightWallGO;

	public void UpdateColliders()
	{
		if (floorGO != null)
		{
			floorGO.GetComponent<MeshCollider>().sharedMesh = null;
			floorGO.GetComponent<MeshCollider>().sharedMesh = 
				floorGO.GetComponent<MeshFilter>().sharedMesh;
		}
		if (leftWallGO != null)
		{
			leftWallGO.GetComponent<MeshCollider>().sharedMesh = null;
			leftWallGO.GetComponent<MeshCollider>().sharedMesh = 
				leftWallGO.GetComponent<MeshFilter>().sharedMesh;
		}
		if (rightWallGO != null)
		{
			rightWallGO.GetComponent<MeshCollider>().sharedMesh = null;
			rightWallGO.GetComponent<MeshCollider>().sharedMesh = 
				rightWallGO.GetComponent<MeshFilter>().sharedMesh;
		}
	}

	public void UpdateMesh()
	{
		Curve curve = GetComponent<PathCreator>().curve;
		Vector3[] points = curve.CalculateEvenlySpacedPoints(spacing);
		int textureRepeat = Mathf.RoundToInt(tiling * points.Length * spacing * 0.5f);

		if(floorGO != null) UpdateFloor(points, textureRepeat);
		if(leftWallGO != null) UpdateLeftWall(points, textureRepeat);
		if(rightWallGO != null) UpdateRightWall(points, textureRepeat);
	}

	void UpdateFloor(Vector3[] points, int textureRepeat)
	{
		floorGO.GetComponent<MeshFilter>().mesh = CreateMesh(points, 0);
		floorGO.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale 
			= new Vector2(1, textureRepeat);
	}

	void UpdateLeftWall(Vector3[] points, int textureRepeat)
	{
		leftWallGO.GetComponent<MeshFilter>().mesh = CreateMesh(points, 1);
		leftWallGO.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale
			= new Vector2(1, textureRepeat);
	}

	void UpdateRightWall(Vector3[] points, int textureRepeat)
	{
		rightWallGO.GetComponent<MeshFilter>().mesh = CreateMesh(points, 2);
		rightWallGO.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale
			= new Vector2(1, textureRepeat);
	}

	// 0 - Floor
	// 1 - Left
	// 2 - Right
	Mesh CreateMesh(Vector3[] points, int version)
	{
		Vector3[] verts = new Vector3[points.Length * 2];
		Vector2[] uvs = new Vector2[verts.Length];
		int[] tris = new int[2 * (points.Length - 1) * 3];

		int vertIndex = 0;
		int triIndex = 0;

		for (int i = 0; i < points.Length; ++i)
		{
			Vector3 forward = Vector3.zero;
			Vector3 p1 = points[i];
			if (i < points.Length - 1)
				forward += points[i + 1] - p1;
			if (i > 0)
				forward += p1 - points[i - 1];
			forward.Normalize();

			//Vector3 left = Vector3.left * forward.y;
			
			Vector3 left = Vector3.Cross(forward, Vector2.up);
			Vector3 up = new Vector3(0, wallHeight, 0);
			if (version == 2) left *= -1;
			verts[vertIndex] = points[i] + left * floorWidth * 0.5f;

			if (version == 0)
			{
				verts[vertIndex + 1] = points[i] - left * floorWidth * 0.5f;
			}
			else if (version == 1 || version == 2)
			{
				verts[vertIndex + 1] = verts[vertIndex] + up;
			}

			float completionPercent = i / (float)(points.Length - 1);
			uvs[vertIndex] = new Vector2(0, completionPercent);
			uvs[vertIndex + 1] = new Vector2(1, completionPercent);

			if (version == 0 || version == 2)
			{
				if (i < points.Length - 1)
				{
					tris[triIndex] = vertIndex;
					tris[triIndex + 1] = vertIndex + 2;
					tris[triIndex + 2] = vertIndex + 1;

					tris[triIndex + 3] = vertIndex + 1;
					tris[triIndex + 4] = vertIndex + 2;
					tris[triIndex + 5] = vertIndex + 3;
				}
			}
			else if (version == 1)
			{
				if (i < points.Length - 1)
				{
					tris[triIndex] = vertIndex + 1;
					tris[triIndex + 1] = vertIndex + 2;
					tris[triIndex + 2] = vertIndex;

					tris[triIndex + 3] = vertIndex + 3;
					tris[triIndex + 4] = vertIndex + 2;
					tris[triIndex + 5] = vertIndex + 1;
				}
			}

			vertIndex += 2;
			triIndex += 6;
		}

		Mesh mesh = new Mesh
		{
			vertices = verts,
			triangles = tris,
			uv = uvs
		};

		return mesh;
	}
}
