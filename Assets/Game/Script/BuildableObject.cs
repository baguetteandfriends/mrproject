﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildableObject : MonoBehaviour {

	public List<Transform> StartPoints;
	public List<Transform> EndPoints;

	// GameObjects that extend off this one
	public List<GameObject> ForwardObject;

	// GameObjects that go into this one
	public List<GameObject> BackwardObject;

	// Might not even need this one but this will 
	// let us specify what type of endpoint it is
	public enum EndType
	{
		None,
		Kill,
		Move
	}

	public EndType endType;

	void Start()
	{
		ForwardObject = new List<GameObject>();
		BackwardObject = new List<GameObject>();
	}

}
