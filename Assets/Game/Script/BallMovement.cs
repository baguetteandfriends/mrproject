﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour
{
	// Ball Movement
	public float zMovement;
	public float moveSpeed;
	public float force;

	// References
	private Transform myTransform;
	private Rigidbody rb;
	public new Camera camera;

	// Used for jumping
	// public float jumpSpeed;
	// public float ballHeight;

	// Useless
	//[SerializeField] float acceleration;

	//	public bool isJumping = false;

	// Our tilt value
	private float xTilt;

	// The velocity we apply to the ball
	private Vector3 velocity;

	void Start()
	{
		//moveSpeed = 5;
		//zMovement = 1.0f;
		//force = 3.0f;
		//jumpSpeed = 10;
		//ballHeight = 0.5f;

		myTransform = transform;
		rb = GetComponent<Rigidbody>();

		// Initial kick so we don't fall off
		rb.AddForce(0, -2, 11, ForceMode.Impulse);
	}

	void LateUpdate()
	{
		// The tilt value
		xTilt = Remap(TiltControl(camera.transform),
			-CameraMovement.tiltMax, CameraMovement.tiltMax, force, -force);

		// I'm not too sure if this is what we want but we normalize it so
		// the ball doesn't move faster when we turn
		velocity = new Vector3(xTilt, 0, zMovement).normalized * moveSpeed;
		rb.AddForce(velocity, ForceMode.Acceleration);

		// Hacky fix but clamp the the y velocity so we can only can go down
// 		rb.velocity = new Vector3(
// 			rb.velocity.x,
// 			Mathf.Clamp(rb.velocity.y, -100, 0),
// 			rb.velocity.z
// 		);

// 		if (Input.GetButtonDown("Jump"))
// 		{
// 			if (Physics.Raycast(myTransform.position, Vector3.down, ballHeight))
// 			{
// 				isJumping = true;
// 				rb.AddForce(0, jumpSpeed, 0, ForceMode.Impulse);
// 			}
// 			else
// 			{
// 				isJumping = false;
// 			}
// 		}
	}

	public float TiltControl(Transform rot)
	{
		float z = rot.transform.eulerAngles.z;
		if (z > 180) z -= 360;
		return z;
	}

	public float Remap(float value, float from1, float to1, float from2, float to2)
	{
		return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
	}
}