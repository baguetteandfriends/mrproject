﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FloorCreator))]
public class FloorEditor : Editor {

	FloorCreator creator;

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		if(GUILayout.Button("Update Colliders"))
		{
			Undo.RecordObject(creator, "Update Colliders");
			creator.UpdateColliders();
		}
	}

	public void OnSceneGUI()
	{
		if(creator.autoUpdate && Event.current.type == EventType.Repaint)
		{
			creator.UpdateMesh();
		}
	}

	public void OnEnable()
	{
		creator = (FloorCreator)target;
	}
}
