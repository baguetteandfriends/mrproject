﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

	public Transform myTransform;
	public BoxCollider bc;

	void Start () {
		bc = GetComponent<BoxCollider>();
		myTransform = transform;
	}

	private void OnTriggerStay(Collider other)
	{
		other.attachedRigidbody.AddForce(myTransform.forward);
		//Camera.main.transform.eulerAngles = myTransform.eulerAngles;
		//other.transform.forward = myTransform.forward;
	}
}