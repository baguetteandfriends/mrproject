﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour
{
	public Transform camTrans;
	public Transform ball;
	public Slider mainSlider;
	public Text angle;
	public Vector3 ballVelocity;
	public Vector2 ballcamOffset;
	public const float tiltMax = 60.0f;

	private Vector2 normalizedVel;
	private Vector3 camPos;
	private Vector3 lerpedValue;

	[Range(-tiltMax, tiltMax)]
	public float zRot;

	void Start()
	{
		mainSlider.minValue = -tiltMax;
		mainSlider.maxValue = tiltMax;
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		// Pretty sure this is just for non vr
		camTrans.LookAt(ball);

		// Get the ball velocity
		// We use this to find out where the camera position should be
		ballVelocity = ball.GetComponent<Rigidbody>().velocity;

		// Normalize and scale the value so we have a consistant distance
		// to the ball
		normalizedVel = new Vector2(
			ballVelocity.x, 
			ballVelocity.z
		).normalized * ballcamOffset.x;

		// Offset the position to the ball
		camPos = new Vector3(
			ball.position.x - normalizedVel.x, 
			ball.position.y + ballcamOffset.y, 
			ball.position.z - normalizedVel.y
		);

		// Lerp the value so we dont do jumpy camera movement
		lerpedValue = Vector3.Lerp(
			camTrans.position, 
			camPos, 
			Time.fixedDeltaTime * 
			Vector3.Distance(camTrans.position, camPos)
		);

		camTrans.position = lerpedValue;

		// This rotates the camera which in turn 
		// rotates the ball in the BallMovement script
		camTrans.eulerAngles = new Vector3(
		camTrans.eulerAngles.x,
		camTrans.eulerAngles.y, zRot);

		zRot = mainSlider.value;
		angle.text = zRot.ToString("F2");
	}

	// Y does this function exist?
	public float GetZRot()
	{
		return zRot;
	}
}