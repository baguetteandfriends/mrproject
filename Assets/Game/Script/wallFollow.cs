﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallFollow : MonoBehaviour {

    public Transform myTransform;
    public Transform follow;
	// Use this for initialization
	void Start () {
        myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
        myTransform.position = new Vector3(myTransform.position.x, myTransform.position.y, follow.position.z+10);
	}
}
