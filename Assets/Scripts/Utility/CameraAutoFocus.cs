﻿using UnityEngine;
using System.Collections;

public class CameraAutoFocus : MonoBehaviour {
	private Transform myTransform;
	public float Distance = 20f;

	void Awake() {
		myTransform = transform;
	}

	void OnEnable() {
		Vector3 newPos = Vector3.zero;
		float angle = Mathf.Deg2Rad * myTransform.localEulerAngles.x;
		newPos.y = Mathf.Sin (angle)  * Distance;
		newPos.z = -Mathf.Cos (angle) * Distance;
		myTransform.localPosition = newPos;
	}

	// Update is called once per frame
	/*void Update () {
		/*Vector3 newPos = Vector3.zero;
		float angle = Mathf.Deg2Rad * myTransform.localEulerAngles.x;
		newPos.y = Mathf.Sin (angle)  * Distance;
		newPos.z = -Mathf.Cos (angle) * Distance;
		myTransform.localPosition = newPos;
	}*/
}
