﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.Speech;
using RMIT.GameTech;

public class SpeechManager : Singleton<SpeechManager>
{
	KeywordRecognizer keywordRecognizer = null;
	Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

	// Use this for initialization
	void Start()
	{
		keywords.Add("Reset ball", () =>
			{
				// Call the OnReset method on every descendant object.
				EventManager.TriggerEvent(EventManager.Events.ResetBall);
			});

		keywords.Add("Start game", () =>
			{
				/*var focusObject = GazeGestureManager.Instance.FocusedObject;
				if (focusObject != null)
				{
					// Call the OnDrop method on just the focused object.
					focusObject.SendMessage("OnDrop");
				}*/
				EventManager.TriggerEvent(EventManager.Events.StartGame);
			});

		keywords.Add("drop ball", () => { TriggerEvent(EventManager.Events.DropBall); });
        keywords.Add("jump", () => { TriggerEvent(EventManager.Events.JumpBall); });
        keywords.Add("start scan", () => { TriggerEvent(EventManager.Events.StartScan); });
        keywords.Add("stop scan", () => { TriggerEvent(EventManager.Events.StopScan); });
        keywords.Add("new game", () => { TriggerEvent(EventManager.Events.NewGame); });

        // Tell the KeywordRecognizer about our keywords.
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());

		// Register a callback for the KeywordRecognizer and start recognizing!
		keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
		keywordRecognizer.Start();
	}
	
	private void TriggerEvent(EventManager.Events eventNum)
	{
		EventManager.TriggerEvent (eventNum);
	}
	
	private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
	{
        Debug.Log(args.text);
		System.Action keywordAction;
		if (keywords.TryGetValue(args.text, out keywordAction))
		{
			keywordAction.Invoke();
		}
	}
}