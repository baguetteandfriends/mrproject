﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PortalType { Floor, Roof, Wall };

public class GamePortal : MonoBehaviour
{
	[SerializeField]
	Transform exitPortalTransform;
	[SerializeField]
	GamePortal exitPortalReference;
	[SerializeField]
	Collider myCollider;
    [SerializeField]
    PortalType portalType;

    private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			Rigidbody rigidBody = other.GetComponent<Rigidbody> ();
			StartCoroutine (exitPortalReference.DisablePortalForSeconds (5f));
			rigidBody.position = new Vector3(exitPortalTransform.transform.position.x, -rigidBody.transform.up.y, exitPortalTransform.transform.position.z);

			SpiderBall spiderBall = other.gameObject.GetComponentInParent<SpiderBall> ();
            switch (portalType)
            {
				case PortalType.Floor:
					spiderBall.gravityNormal = Vector3.up;
                    break;
                case PortalType.Roof:
					spiderBall.gravityNormal = -Vector3.up;
                    break;
                case PortalType.Wall:
					//spiderBall.gravityNormal = Vector3.up;
                    break;
                default:
                    break;
            }
		}
	}

    //Get the exit portals transform so we can teleport to it
    public void SetExitPortalTransform(Transform trans)
	{
        exitPortalTransform = trans;
	}

    //Get a reference to the out portals Script so we can call start its DisablePortalForSeconds() corroutine
    public void SetExitPortalReference(GamePortal p)
	{
        exitPortalReference = p;
	}

	public void SetPortalType(PortalType type)
	{
		portalType = type;
	}
        
    //Disables this portals trigger so when the ball teleports here it doesn't get sent straight back
	IEnumerator DisablePortalForSeconds(float t)
	{
		myCollider.enabled = false;

		while (t > 0) 
		{
			t -= Time.deltaTime;
            yield return null;
        }

		myCollider.enabled = true;
	}
}
