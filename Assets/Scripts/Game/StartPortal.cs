﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;

public class StartPortal: Portal {
	public EndPortal endPortal;
    public Vector3 startGravity;

	// Use this for initialization
	new void Start () {
        base.Start();
        SpiderBall ball = BallManager.Instance.SpawnBall(transform.position + (-startGravity * 0.025f), -startGravity, GazeManager.Instance);
        ball.startPortal = this;
		ball.endPortal = endPortal;
        ball.gravityNormal = startGravity;
        ball.ResetBall();
	}
	
}
