﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum FanState { IDLE, SLOW, FAST };

public class fan2 : MonoBehaviour {

	//FanState state;
    Animator anim;
	public Material emissive;
	public FanParticleController particleController;
	public Material normal;
	private float dist;
	public float forceClose;
	public float forceFar;
	public MeshRenderer[] meshRenderers;


    void Start() {
		//state = FanState.IDLE;
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            anim.SetTrigger("Range");

			foreach (MeshRenderer r in meshRenderers) 
			{
				r.material = emissive;
			}
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
			dist = Vector3.Distance (this.transform.position, other.transform.position);

			Rigidbody rigidBody = other.GetComponent<Rigidbody> ();
			 	 
			if (dist < 1.5) {
				anim.SetTrigger ("Affect");
				rigidBody.AddForce ((transform.forward) * forceClose, ForceMode.Acceleration);
				particleController.PlayExplosion ();
			} else {
				rigidBody.AddForce ((transform.forward) * forceFar, ForceMode.Acceleration);
			}
				
        }
    }

	void OnTriggerExit(Collider other)
	{
		foreach (MeshRenderer r in meshRenderers) 
		{
			r.material = normal;
		}
	}
}
