﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RMIT.GameTech;
using HoloToolkit.Unity.InputModule;

public class BallManager : Singleton<BallManager> {
	public GameObject BallPrefab;

    private List<SpiderBall> balls;

    public void Start()
    {
        balls = new List<SpiderBall>();

        // Delete ALL children.
        for(int i = 0; i < transform.childCount; ++i)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    [ContextMenu("Spawn Ball")]
    public void SpawnBall()
    {
        SpawnBall(Vector3.zero, Vector3.up, GazeManager.Instance);
        
    }

    public SpiderBall GetFirstBall()
    {
        if (balls == null || balls.Count < 1)
            return null;

        return balls[0];
    }

    public void ClearBalls()
    {
		if (balls == null)
			return;
		
        // Delete ALL Balls.
        for(int i = 0; i < balls.Count; ++i)
        {
			if (balls[i] != null)
            	Destroy(balls[i].gameObject);
        }

		balls.Clear ();
    }

    public SpiderBall SpawnBall(Vector3 position, Vector3 up, GazeManager gazeManager = null)
    {
        GameObject obj = GameObject.Instantiate(BallPrefab, null, false);
        obj.transform.position = position;
        obj.transform.up = up;
        SpiderBall spiderBall = obj.GetComponent<SpiderBall>();
        spiderBall.gazeManager = gazeManager;
        
        if (PointerManager.Instance != null)
        {
            PointerManager.Instance.SpawnPointer(spiderBall.rigidBody.transform);
        }

        balls.Add(spiderBall);
        return spiderBall;
    }

    public void FixedUpdate()
    {
        for(int i = 0; i < balls.Count; ++i)
        {
            balls[i].UpdateMe();
        }
    }

    public void LateUpdate()
    {
        for(int i = 0; i < balls.Count; ++i)
        {
            balls[i].LateUpdateMe();
        }
    }
}
