﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {
    public Animator animator;
    public string EventString;
    public string PortalTag;
    public bool startOpened;

    private bool IsOpened;

    // Use this for initialization
    protected void Start() {
        if (startOpened)
        {
            SetOpened(true);
        }
    }

    // Update is called once per frame
    /*void Update() {

    }*/

    [ContextMenu("Open")]
    protected void Open()
    {
        SetOpened(true);
    }

    [ContextMenu("Close")]
    protected void Close()
    {
        SetOpened(false);
    }

    public void SetOpened(bool opened)
    {
        IsOpened = opened;
        UpdateAnims();
    }

    protected void UpdateAnims()
    {
        if (animator != null)
        {
            animator.SetBool("IsOpened", IsOpened);
        }
    }

    protected void OnTriggerEnter(Collider other)
    {
		if (EventString.Length > 0)
        	EventManager.TriggerEvent(EventString);
    }
}
