﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeatureSpawner : MonoBehaviour
{
    public GameObject[] features;
    /* Features by Index:
     * 0 - Jump Pad
     * 1 - Bump Pad
     * 2 - Fan
     * 3 - Big Trap
     * 4 - Small Trap
     */

    public GameObject[] portals;
    /* Portals by Index:
     * 0 - Blue Portal
     * 1 - Orange Portal
     */

    //Instantiates a selected by index (0-4) feature at the provided position with the provided orientation
    public void SpawnFeature(int featureIndex, Vector3 position, Quaternion orientation)
    {
		Instantiate(features[featureIndex], position, orientation);
    }

    //Instantiates a random map feature at the provided position with the provided orientation
    public void SpawnRandomFeature(Vector3 position, Quaternion orientation)
    {
        int featureIndex = Random.Range(0, features.Length);
        Instantiate(features[featureIndex], position, orientation);
    }

    //Instantiates two portal prefabs (one blue, one orange) at the provided positions with the provided orientations, and set each as the others exit portal
    public void SpawnPortals(Vector3 position1, Quaternion orientation1, PortalType type1, Vector3 position2, Quaternion orientation2, PortalType type2)
    {
		GameObject bluePortal = Instantiate(portals[0], position1, orientation1);
        GamePortal bP = bluePortal.GetComponent<GamePortal>();
		bP.SetPortalType (type1);
        GameObject orangePortal = Instantiate(portals[1], position2, orientation2);
        GamePortal oP = orangePortal.GetComponent<GamePortal>();
		oP.SetPortalType (type2);

        bP.SetExitPortalTransform(orangePortal.transform);
        bP.SetExitPortalReference(oP);
        oP.SetExitPortalTransform(bluePortal.transform);
        oP.SetExitPortalReference(bP);
    }
}
