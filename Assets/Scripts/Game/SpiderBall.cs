﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA.Input;
using HoloToolkit.Unity.InputModule;
using System;

public class SpiderBall : MonoBehaviour, IInputHandler, IInputClickHandler
{
	public Rigidbody rigidBody;
	public SphereCollider sphereCollider;
    public Transform trailTransform;
    public Transform ModelRoot;
    public GameObject targetGO;
	public LayerMask hitMask;
    public Animator animator;

	public Vector3 gravityNormal;
	public const float gravityMax = 9.86f;
	public float gravityAmount = 0f;

	private Vector3 moveVector;
	private Vector3 moveTangent;
    private Vector3 forwardNormal;
    private Vector3 rightNormal;
	private bool isHolding = false;
    private bool isOnGround = false;
    private bool jump = false;
	public GazeManager gazeManager;
	private float radius;
	private float movedDistance;

    public StartPortal startPortal;
	public EndPortal endPortal;

    public GameObject EndPointerPrefab;
	private Pointer endPortalPointer;

    private int anim_VelocityHash = Animator.StringToHash("Velocity");
    private int anim_IsHoldingHash = Animator.StringToHash("IsHolding");
    private int anim_IsOnGroundHash = Animator.StringToHash("IsOnGround");

    void Awake() {
		EventManager.StartListening (EventManager.Events.ResetBall, ResetBall);
		EventManager.StartListening (EventManager.Events.DropBall, DropBall);
        EventManager.StartListening (EventManager.Events.JumpBall, JumpBall);
		gravityNormal = Vector3.zero;
		moveVector = Vector3.zero;
		radius = sphereCollider.radius;
        
	}

	private void Start()
	{
        InputManager.Instance.AddGlobalListener (gameObject);
        forwardNormal = rigidBody.transform.forward;
        rightNormal = rigidBody.transform.right;

    }

	public void OnInputDown(InputEventData eventData) {
		isHolding = true;
	}

	public void OnInputUp(InputEventData eventData) {
		isHolding = false;
	}

    public void OnInputClicked(InputClickedEventData eventData)
    {
        //JumpBall();
    }

    private void JumpBall()
    {
        jump = true;
    }

    private void OnDestroy()
	{
		InputManager.Instance.RemoveGlobalListener (gameObject);
		EventManager.StopListening (EventManager.Events.ResetBall, ResetBall);
		EventManager.StopListening (EventManager.Events.DropBall, DropBall);
	}
		
	void Update() {
		if (Input.GetKeyDown (KeyCode.D)) {
			DropBall ();
		}

		if (Input.GetKeyDown (KeyCode.R)) {
			ResetBall ();
		}

		if (endPortalPointer == null && endPortal != null) {
			if (PointerManager.Instance != null) {
				endPortalPointer = PointerManager.Instance.SpawnPointer (endPortal.transform, EndPointerPrefab);
				//endPortalPointer.transform.localScale = Vector3.one * 0.5f;
			}
		}
	}

	const float RayCastExtraDistance = 0.02f;
#if false // DD: OLD STUFF
    bool FindNewGravitySurface(out RaycastHit hitInfo)
	{
		// We first raycast in the direction the ball is rolling. if hit a 90 degree surface is detected set that as the new 'gravity'.
		if (CheckForWallInfront(out hitInfo)== false)
		{
			// Second we make sure there is something BELOW us.
			return RecursiveCurveGravitySearch(0f, out hitInfo);
		}
		Debug.DrawRay (rigidBody.position, (hitInfo.point - rigidBody.position), Color.magenta, 3f);
		return true;
	}

	bool CheckForWallInfront(out RaycastHit hitInfo)
	{
        hitInfo = new RaycastHit();
        return false;
        //return Physics.Raycast(rigidBody.position, )
        //return Physics.SphereCast (rigidBody.position + (-gravityNormal * RayCastDistance), radius, moveVector, out hitInfo, RayCastDistance, hitMask);
    }

	bool RecursiveCurveGravitySearch(float angle, out RaycastHit hitInfo)
	{
		if (angle > 90f) {
			hitInfo = new RaycastHit ();
			return false;
		} else {
			if (Physics.SphereCast(rigidBody.position, radius, Vector3.RotateTowards(gravityNormal, moveTangent, -(Mathf.Deg2Rad * angle), 0f), out hitInfo, RayCastExtraDistance * 2f, hitMask) == false)
				return RecursiveCurveGravitySearch (angle += 1f, out hitInfo);
			Debug.DrawRay (rigidBody.position, (hitInfo.point - rigidBody.position), Color.cyan, 3f);
			return true;
		}
	}
#endif

    public void UpdateMe()
    //void FixedUpdate()
    {
        if (rigidBody != null && gazeManager != null)
        {
            // Handle Holding State.
            if (isHolding)
            {
                moveVector = gazeManager.HitPosition - rigidBody.position;
                
                gravityAmount = gravityMax;
                moveTangent = moveVector.normalized;

                rightNormal = Vector3.Cross(moveVector.normalized, gravityNormal);
                //Debug.DrawLine(rigidBody.position, rigidBody.position + rightNormal, Color.cyan);
                moveTangent = Vector3.Cross(gravityNormal, rightNormal);
                //Debug.DrawLine(rigidBody.position, rigidBody.position + moveTangent, Color.green);
                forwardNormal = moveTangent;
            }
            else
            {
                moveTangent = Vector3.zero;
            }

            // Clamp the gravity amount between 0 -> real-life g constant.
            gravityAmount = Mathf.Clamp(gravityAmount, 0f, gravityMax);
            
            isOnGround = Physics.CheckSphere(rigidBody.position, radius + (RayCastExtraDistance * 2f), hitMask, QueryTriggerInteraction.Ignore);

            Debug.DrawLine(rigidBody.position, rigidBody.position + (gravityNormal * radius), Color.red, 1f);

            bool applyMovement = false;
            float minDistanceToMove = 0.05f;
            float maxDistanceToMove = 3f;
            if (gravityNormal.sqrMagnitude > 0f && moveTangent.sqrMagnitude > 0f &&
                moveVector.sqrMagnitude > minDistanceToMove * minDistanceToMove &&
                moveVector.sqrMagnitude < maxDistanceToMove * maxDistanceToMove)
            {
                applyMovement = true;
                rigidBody.AddForce(moveTangent * (gravityMax * 0.65f), ForceMode.Acceleration);
            }

            // Apply the gravity if we aren't touching a surface or moving
            if (!isOnGround || (isHolding && applyMovement)) 
            {
                rigidBody.AddForce(gravityNormal * gravityAmount, ForceMode.Acceleration);
                Debug.DrawRay(rigidBody.position, gravityNormal * 1.0f, Color.blue, 1f);
            }
                
            // Handle Jumping
            float jumpAmount = 2.5f;
            if (jump && isOnGround)
            {
                jump = false;
                rigidBody.AddForce(-gravityNormal * jumpAmount, ForceMode.Impulse);
            }

        }
    }

    // Handle all the animation stuff and do gravity checks before next FixedUpdate
    public void LateUpdateMe()
    {
        if (animator)
        {
            float velocityMagnitude = rigidBody.velocity.magnitude;
            animator.SetFloat(anim_VelocityHash, velocityMagnitude);
            animator.SetBool(anim_IsHoldingHash, isHolding);
            animator.SetBool(anim_IsOnGroundHash, isOnGround);

            //ModelRoot.position = rigidBody.position;
            if (velocityMagnitude > 0.05f || isHolding)
            {
                ModelRoot.rotation = rigidBody.rotation;
            }
            else
            {
                Debug.DrawRay(rigidBody.position, forwardNormal, Color.gray, 1f);
				ModelRoot.rotation = Quaternion.LookRotation (forwardNormal, -gravityNormal);
                ModelRoot.Rotate(Vector3.up, 90f, Space.Self);
            }
        }

        //trailTransform.forward = forwardNormal;
        trailTransform.position = rigidBody.position;

        // First check if we are climbing a 'new wall'
        RaycastHit hitInfo;
        if (Physics.SphereCast(rigidBody.position, radius, forwardNormal, out hitInfo, RayCastExtraDistance, hitMask, QueryTriggerInteraction.Ignore))
        {
            gravityNormal = -hitInfo.normal;
        }
        else if (Physics.Raycast(rigidBody.position, gravityNormal,out hitInfo, RayCastExtraDistance * 2f, hitMask) == false) // Second check if we are falling off a cliff
        {
            if (Physics.SphereCast(rigidBody.position, radius + (RayCastExtraDistance * 2f), gravityNormal, out hitInfo, RayCastExtraDistance, hitMask, QueryTriggerInteraction.Ignore))
            {
                Debug.DrawLine(hitInfo.point, hitInfo.point + (hitInfo.normal), Color.yellow, 1f);
                gravityNormal = -hitInfo.normal;
            }
        }
        else
        {
            gravityNormal = -hitInfo.normal;
        }

    }
    
    public void ResetBall()
    {
        if (startPortal != null)
        {
            rigidBody.position = startPortal.transform.position + (-startPortal.startGravity * radius);
            gravityNormal = startPortal.startGravity;
            gravityAmount = gravityMax;
        }
        else { 
            rigidBody.position = Vector3.zero;
            gravityNormal = Vector3.zero;
        }
	}

	void DropBall()
	{
		gravityNormal = Vector3.down;
		gravityAmount = gravityMax;
	}
}
