﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pointer : MonoBehaviour {
    public Transform Target;
    public GameObject Model;
    private Transform myTransform;
    private Transform cameraTransform;
    private Camera mainCamera;

    [Tooltip("Allowable percentage inside the holographic frame to continue to show a directional indicator.")]
    [Range(-0.3f, 0.3f)]
    public float VisibilitySafeFactor = 0.1f;

    [Tooltip("Multiplier to decrease the distance from the cursor center an object is rendered to keep it in view.")]
    [Range(0.1f, 1.0f)]
    public float MetersFromCursor = 0.3f;

    // Use this for initialization
    void Start () {
        myTransform = transform;
        cameraTransform = Camera.main.transform;
        mainCamera = Camera.main;
    }
	
	// Update is called once per frame
	void Update () {
        if (Target != null)
        {
            //myTransform.up = (Target.position - myTransform.position).normalized;

            // Direction from the Main Camera to this script's parent gameObject.
            Vector3 camToObjectDirection = Target.position - cameraTransform.position;
            camToObjectDirection.Normalize();

            // The cursor indicator should only be visible if the target is not visible.
            bool isDirectionIndicatorVisible = !IsTargetVisible(Target.position);
            Model.SetActive(isDirectionIndicatorVisible);

            if (isDirectionIndicatorVisible)
            {
                Vector3 rotation;
                GetDirectionIndicatorPositionAndRotation(
                    camToObjectDirection,
                    out rotation);
                myTransform.up = rotation;
            }
        }
        else
        {
            Model.SetActive(false);
			Destroy (gameObject);
        }
	}

    private bool IsTargetVisible(Vector3 position)
    {
        // This will return true if the target's mesh is within the Main Camera's view frustums.
        Vector3 targetViewportPosition = mainCamera.WorldToViewportPoint(position);
        return (targetViewportPosition.x > VisibilitySafeFactor && targetViewportPosition.x < 1 - VisibilitySafeFactor &&
                targetViewportPosition.y > VisibilitySafeFactor && targetViewportPosition.y < 1 - VisibilitySafeFactor &&
                targetViewportPosition.z > 0);
    }

    private void GetDirectionIndicatorPositionAndRotation(Vector3 camToObjectDirection, /*out Vector3 position*/ out Vector3 rotation)
    {
        // Find position:
        // Save the cursor transform position in a variable.
        Vector3 origin = myTransform.position;

        // Project the camera to target direction onto the screen plane.
        Vector3 cursorIndicatorDirection = Vector3.ProjectOnPlane(camToObjectDirection, -1 * cameraTransform.forward);
        cursorIndicatorDirection.Normalize();

        // If the direction is 0, set the direction to the right.
        // This will only happen if the camera is facing directly away from the target.
        if (cursorIndicatorDirection == Vector3.zero)
        {
            cursorIndicatorDirection = cameraTransform.right;
        }

        // The final position is translated from the center of the screen along this direction vector.
        //position =  origin + cursorIndicatorDirection * MetersFromCursor;

        // Find the rotation from the facing direction to the target object.
        rotation = cursorIndicatorDirection;//Quaternion.LookRotation(cameraTransform.forward, cursorIndicatorDirection);// * directionIndicatorDefaultRotation;
    }
}
