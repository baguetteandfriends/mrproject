﻿
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class PlexusEffect : MonoBehaviour
{
    Transform _transform;

    float maxDistance = 0.25f;
    float maxDistancerSqr;
    Vector3 firstParticlesPosition;
    Vector3 secondParticlesPosition;
    float distanceSqr;

    new ParticleSystem particleSystem;
    ParticleSystem.Particle[] particles;
    ParticleSystem.MainModule particleSystemMainModule;
    int maxParticles;
    int particleCount;

    public LineRenderer lineRendererTemplate;
    public List<LineRenderer> lineRenderers = new List<LineRenderer>();
    int lrIndex;
    int lrCount;
    int maxLineRenderers = 25;
    int maxConnections = 3;
    int connections;

    void Start ()
    {
        _transform = transform;
        particleSystem = GetComponent<ParticleSystem>();
        particleSystemMainModule = particleSystem.main;
    }

	void FixedUpdate ()
    {
        maxParticles = particleSystemMainModule.maxParticles;

        if(particles == null || particles.Length < maxParticles)
        {
            particles = new ParticleSystem.Particle[maxParticles];
        }

        particleSystem.GetParticles(particles);
        particleCount = particleSystem.particleCount;
        maxDistancerSqr = maxDistance * maxDistance;

        lrIndex = 0;
        lrCount = lineRenderers.Count;

        for (int i = 0; i < particleCount; i++)
        {
            if(lrIndex == maxLineRenderers)
            {
                break;
            }

            firstParticlesPosition = particles[i].position;
            connections = 0;

            for (int j = i+1; j < particleCount; j++)
            {
                secondParticlesPosition = particles[j].position;

                distanceSqr = Vector3.SqrMagnitude(firstParticlesPosition - secondParticlesPosition);

                if (distanceSqr <= maxDistancerSqr)
                {
                    LineRenderer lr;

                    if (lrIndex == lrCount)
                    {
                        lr = Instantiate(lineRendererTemplate, _transform, false);
                        lineRenderers.Add(lr);
                        lrCount++;
                    }

                    lr = lineRenderers[lrIndex];

                    lr.enabled = true;
                    lr.SetPosition(0, firstParticlesPosition);
                    lr.SetPosition(1, secondParticlesPosition);
                    lrIndex++;
                    connections++;

                    if (connections == maxConnections || lrIndex == maxLineRenderers)
                    {
                        break;
                    }
                }
            }
        }

        for (int i = lrIndex; i < lineRenderers.Count; i++)
        {
            lineRenderers[i].enabled = false;
        }
    }
}
