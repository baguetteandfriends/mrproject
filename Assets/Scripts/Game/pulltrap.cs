﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pulltrap : MonoBehaviour {
    public bool alreadytrapped = false;
	public Color emissionColor;
	public MeshRenderer[] MeshParts;

    private void OnTriggerEnter(Collider other)     
    {
		if (other.GetComponent<Player>().istrapped == false){
            if (other.tag == "Player" && alreadytrapped == false)
            {
                other.GetComponent<Rigidbody>().velocity = Vector3.zero;
                other.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                other.transform.position = new Vector3(this.transform.position.x, 1, this.transform.position.z) ;
				other.GetComponent<Player>().istrapped = true;
                alreadytrapped = true;


				foreach (MeshRenderer r in MeshParts) {
					r.material.SetColor("_EmissionColor", emissionColor);
				}
            }
        }
    }
	private void OnTriggerExit (Collider other){

			foreach (MeshRenderer r in MeshParts) {
				r.material.SetColor("_EmissionColor", Color.black);
			}
		other.GetComponent<Player>().istrapped = false;
		alreadytrapped = false;
}
}