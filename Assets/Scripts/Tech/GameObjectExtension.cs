﻿using UnityEngine;
using System.Collections;

public static class GameObjectExtension {

	public static void SetLayer(this GameObject parent, int layer, bool includeChildren = true)
	{
		parent.layer = layer;
		if (includeChildren)
		{
			foreach (Transform trans in parent.transform.GetComponentsInChildren<Transform>(true))
			{
				trans.gameObject.layer = layer;
			}
		}
	}

	public static void ChangeLayer(this GameObject parent, int fromLayer, int toLayer, bool includeChildren = true)
	{
		if (parent == null)
			return;
		
		if (parent.layer == fromLayer)
			parent.layer = toLayer;


		Transform localTrans = parent.transform;

		if (includeChildren)
		{
			for (int i = 0; i < localTrans.childCount; ++i) {
				localTrans.GetChild (i).gameObject.ChangeLayer (fromLayer, toLayer, includeChildren);
			}
		}
	}
}