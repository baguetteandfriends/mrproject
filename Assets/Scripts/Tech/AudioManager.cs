﻿///Summary
///	All game audio is played through this Audio Manager
///	Create an AudioEvent, specify the clip, channel and any other settings then play it with AudioManager.Instance.PlayAudio()
///	See the test function below for an example implementation

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using RMIT.GameTech;

public class AudioManager : Singleton<AudioManager> {

	public AudioClip[] music, soundEffects;

	public AudioMixer masterMixer;
	public AudioMixerGroup[] mixerGroups;

	[Header("Starting volume")]
	public float effectsVolume;
	public float musicVolume;

	public static bool effectsOn = true;
	public static bool musicOn = true; //TODO: read these from preferences
	
	private AudioSource[] channels;

	public enum mixerChannel {
		Master	= 0,
		Music	= 1,
		UI		= 2,
		Foley	= 3
	};

	public enum audioPriority {
		high,	// stop any other sounds playing in this channel and play this one
		low,	// don't play this sound if the channel is already playing
		mix		// play this sound over any other sounds already playing in the same channel - default
	};

	public class AudioEvent {

		public string name;
		public bool looping;
		public float volume;
		public mixerChannel channel;
		public audioPriority priority;
		public Vector3 position;

		/// <summary>
		/// Initializes a new instance of the <see cref="AudioManager+AudioEvent"/> class.
		/// </summary>
		/// <param name="_name">Name.</param>
		/// <param name="_channel">Channel.</param>
		public AudioEvent (string _name, mixerChannel _channel)
		{
			name = _name;
			channel = _channel;
			looping = false;
			volume = 1;
			priority = audioPriority.mix;
			position = Vector3.zero;
		}

		public AudioEvent(string _name, mixerChannel _channel, audioPriority _priority)
		{
			name = _name;
			channel = _channel;
			looping = false;
			volume = 1;
			priority = _priority;
			position = Vector3.zero;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AudioManager+AudioEvent"/> class with specified settings.
		/// </summary>
		/// <param name="_name">Name.</param>
		/// <param name="_channel">Channel.</param>
		/// <param name="_looping">Looping.</param>
		/// <param name="_volume">Volume (range 0 to 1).</param>
		public AudioEvent (string _name, mixerChannel _channel, audioPriority _priority, bool _looping, float _volume)
		{
			name = _name;
			channel = _channel;
			looping = _looping;
			volume = _volume;
			priority = _priority;
			position = Vector3.zero;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AudioManager+AudioEvent"/> class with specified settings and position.
		/// </summary>
		/// <param name="_name">Name.</param>
		/// <param name="_channel">Channel.</param>
		/// <param name="_looping">Looping</param>
		/// <param name="_volume">Volume (range 0 to 1).</param>
		/// <param name="_position">Position in world space.</param>
		public AudioEvent (string _name, mixerChannel _channel, audioPriority _priority, bool _looping, float _volume, Vector3 _position)
		{
			name = _name;
			channel = _channel;
			looping = _looping;
			volume = _volume;
			priority = _priority;
			position = _position;
		}
	}

	void Awake ()
	{
		// one audiosource (channel) for each group in the mixer
		channels = new AudioSource[4];

		for (int i = 0; i < channels.Length; i++)
		{
			channels[i] = gameObject.AddComponent<AudioSource>() as AudioSource;
			channels[i].outputAudioMixerGroup = mixerGroups[i];
		}

		EventManager.StartListening("TransitionToDay", PlayDayMusic);
		EventManager.StartListening("TransitionToNight", PlayNightMusic);

		SetChannelVolume(mixerChannel.Music, musicVolume);
		SetChannelVolume(mixerChannel.Foley, effectsVolume);
		SetChannelVolume(mixerChannel.UI, effectsVolume);
	}
	
	public void ToggleEffects()
	{
		effectsOn = !effectsOn;
		if (effectsOn) {
			SetChannelVolume(mixerChannel.Foley, effectsVolume);
			SetChannelVolume(mixerChannel.UI, effectsVolume);
		} else {
			SetChannelVolume(mixerChannel.Foley, -80);
			SetChannelVolume(mixerChannel.UI, -80);
		}
	}

	public void ToggleMusic()
	{
		musicOn = !musicOn;
		if (musicOn)
			SetChannelVolume(mixerChannel.Music, musicVolume);
		else {
			SetChannelVolume(mixerChannel.Music, -80);
		}
	}

	public void PlayAudio(AudioEvent ae)
	{
		AudioClip clip = null;

		if (ae.channel == mixerChannel.Music) {
			foreach (AudioClip effect in music) {
				if (effect.name == ae.name) {
					clip = effect;
				}
			}
		} else {
			foreach (AudioClip effect in soundEffects) {
				if (effect.name == ae.name) {
					clip = effect;
				}
			}
		}

		if (clip == null) {
			Debug.LogWarning("Effect '" + ae.name + "' not found");
			return;
		}

		if (ae.position != Vector3.zero)
		{
			//TODO: handle 3D audio attenuation if required
		}

		// check priority
		if (ae.priority == audioPriority.high && channels[(int)ae.channel].isPlaying) // stop the current sound
			channels[(int)ae.channel].Stop();
		else if (ae.priority == audioPriority.low && channels[(int)ae.channel].isPlaying) // don't interrupt the current sound
			return;
		
		channels[(int)ae.channel].clip = clip;
		channels[(int)ae.channel].loop = ae.looping;
		channels[(int)ae.channel].volume = ae.volume;

		channels[(int)ae.channel].Play();
	}

	public void PlayMusic (int trackNumber, bool looping)
	{
		// only one music track can play at a time
		if (channels[(int)mixerChannel.Music].isPlaying)
			channels[(int)mixerChannel.Music].Stop();

		channels[(int)mixerChannel.Music].clip = music[trackNumber];
		channels[(int)mixerChannel.Music].loop = looping;

		channels[(int)mixerChannel.Music].Play();			
	}

	private void PlayDayMusic ()
	{
		PlayMusic(0, true);
	}

	private void PlayNightMusic ()
	{
		PlayMusic(1, true);
	}

	/// <summary>
	/// Sets the channel volume.
	/// </summary>
	/// <param name="channel">Channel.</param>
	/// <param name="volume">Volume (in dB)</param>
	public void SetChannelVolume (mixerChannel channel, float volume)
	{
		masterMixer.SetFloat (channel.ToString() + "Volume", volume);
	}


	[ContextMenu("Play test audio")]
	private void Test ()
	{
		// this is an example of how to play audio using this AudioManager

		AudioEvent newSound = new AudioEvent ("test", mixerChannel.UI, audioPriority.mix, false, 1);
		AudioManager.Instance.PlayAudio (newSound);

		//SetChannelVolume (mixerChannel.Foley, -10f);
	}

}
